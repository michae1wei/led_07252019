
Firmware for LED board v.07252019
===================================

---

## Overview

Based on uC **STM32f030C8T6**  
The code reads command from mainboard and set color for individual LED or all LED at once on the LED board.  
Basically everything happens in 2 files: [usart2.c](https://bitbucket.org/michae1wei/led_07252019/src/master/X/usart2.c?at=master), and [main.c](https://bitbucket.org/michae1wei/led_07252019/src/master/USER/main.c?at=master).

---

## Read incoming serial commands

The format of the commands is: **(# of LED/all LED)-(space)-(red value)-(space)-(green value)-(space)-(blue value)-(space)**  
		For example:  **'0 255 0 255 '** //set 0th LED to color 255 0 255  
		or:           **'a 0 25 125  '**  //set all LED to color 0 25 125   

Each character of a command (including space) triggers a serial interrupt, and the `usart2_interrupt(void)` handles each character input:  
  
### LED number or all LEDS:

```c
if (get_input == 0x61)//a
{
	all = 1;
	ledct = 0;
	ct--;
	break;
}
else
{
	all = 0;
	ledct = get_input - 0x30;
	ct--;
	break;	
}
```  
### Store color value into `clr[]`, and get color when space input(blue for example):  

```c

if (get_input == 0x20)
{

	if (all)
	{
		for (ledct = 0; ledct < 8; ledct++)
		{
			b[ledct] = get_color(clr, numct);
		}
	}
	else
	{
		b[ledct] = get_color(clr, numct);
	}
	numct = 0;
	ct = 4;
	
}
else
{
	clr[numct] = get_input-0x30;
	numct++;
}
```

These code are located in [usart2.c](https://bitbucket.org/michae1wei/led_07252019/src/master/X/usart2.c?at=master).  

---

## Send commands to control LEDs  
The series data structure of LED shown as follow:  

![](https://wx1.sinaimg.cn/mw690/006NaHYily1g5j3k4t1ovj312c0gijtx.jpg)  
### Follow the structure to send data in the infinite loop:
```c
while(1)
{
	timeout++;
	if(timeout > 10)
	{
		ct = 4;
		timeout = 0;
	}
	Send_Digits(0);
	Send_Digits(0);
	Send_Digits(0);
	Send_Digits(0);
	
	Send_Color(r[0],g[0],b[0]);
	Send_Color(r[1],g[1],b[1]);
	Send_Color(r[2],g[2],b[2]);
	Send_Color(r[3],g[3],b[3]);
	Send_Color(r[4],g[4],b[4]);
	Send_Color(r[5],g[5],b[5]);
	Send_Color(r[6],g[6],b[6]);
	Send_Color(r[7],g[7],b[7]);
	
	Delay(200);
	Send_Digits(1);
	Send_Digits(1);
	Send_Digits(1);
	Send_Digits(1);
	GPIO_Write(GPIOB, 0);
}
```

### Send bytes along with the clock:  
```c
void Send_Digits(uint8_t digits)
{
	int control_bits;
	for(control_bits = 7; control_bits < 15; control_bits++)
	{
		GPIOB->ODR=digits<< control_bits ;
		GPIO_SetBits(GPIOB,GPIO_Pin_13);
		GPIO_ResetBits(GPIOB,GPIO_Pin_13);
	}
}
```
These code are located in [main.c](https://bitbucket.org/michae1wei/led_07252019/src/master/USER/main.c?at=master).  

---
## Other files related but not that essential  
+ [led.c](https://bitbucket.org/michae1wei/led_07252019/src/master/X/led.c) //Initialize GPIO for LED control  
+ [stm32f0xx_it.c](https://bitbucket.org/michae1wei/led_07252019/src/master/USER/stm32f0xx_it.c) //Where the interrupt happens
+ [LED.hex](https://bitbucket.org/michae1wei/led_07252019/src/master/Output/LED.hex) //Download this to flash the chip

