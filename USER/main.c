#include "x.h"
#include "usart2.h"
#include "led.h"
#include "colors.h"
#include "stm32f0xx_misc.h"
#ifdef USE_FULL_ASSERT
void assert_failed(uint8_t * file, uint32_t line)
{ 
	while(1)
	{
	}
}
#endif

	void Delay(__IO uint32_t nCount);
  void Send_Digits(uint8_t digits);
  void Send_Color(uint8_t red, uint8_t blue, uint8_t green);
	int r[8], g[8], b[8],ledct,i, timeout;
	extern int ct;
	
int main(void)
{
	/* Initial system clock - 48MHZ */
	SystemInit();

	/* Initial Uart */
	usart2_initial();

	/* Initial Control Pins */
	led_initial();
	
	for(i = 0; i < 8; i++) // Set initial color blue
	{
		r[i] = 0;
		b[i] = 0xFF;
		g[i] = 0;
	}

	
	while(1)
	{
		timeout++;
		if(timeout > 10)
		{
			ct = 4;
			timeout = 0;
		}
		Send_Digits(0);
	  Send_Digits(0);
	  Send_Digits(0);
	  Send_Digits(0);
		
		Send_Color(r[0],g[0],b[0]);
		Send_Color(r[1],g[1],b[1]);
		Send_Color(r[2],g[2],b[2]);
		Send_Color(r[3],g[3],b[3]);
		Send_Color(r[4],g[4],b[4]);
		Send_Color(r[5],g[5],b[5]);
		Send_Color(r[6],g[6],b[6]);
		Send_Color(r[7],g[7],b[7]);
		
		Delay(200);
		Send_Digits(1);
	  Send_Digits(1);
	  Send_Digits(1);
	  Send_Digits(1);
		GPIO_Write(GPIOB, 0);
	}
}


void Send_Digits(uint8_t digits)
{
	int control_bits;
	for(control_bits = 7; control_bits < 15; control_bits++)
	{
		GPIOB->ODR=digits<< control_bits ;
		GPIO_SetBits(GPIOB,GPIO_Pin_13);
		GPIO_ResetBits(GPIOB,GPIO_Pin_13);
	}
}

void Send_Color(uint8_t red, uint8_t green, uint8_t blue)
{
	
		Send_Digits(0xE0 | 30);
		Send_Digits(blue);
		Send_Digits(green);
		Send_Digits(red);
}

void Delay(__IO uint32_t nCount)
{
  for(;nCount!=0;nCount--);
}

