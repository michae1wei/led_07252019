#include "led.h"


void led_initial(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
  
    /* Configure PB.13, PB.14 IO in output push-pull mode to drive external LED */
	GPIO_InitStruct.GPIO_Pin = (GPIO_Pin_13 | GPIO_Pin_14);
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOB, &GPIO_InitStruct); 

	GPIO_ResetBits(GPIOB, GPIO_Pin_13 | GPIO_Pin_14);
}
