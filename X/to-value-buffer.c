#include "to-value-buffer.h"

uint32_t to_value32(uint8_t * buf)
{
	uint32_t val;

	val = (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | (buf[3] << 0);
	return val;
}

uint16_t to_value16(uint8_t * buf)
{
	uint16_t val;

	val = (buf[0] << 8) | (buf[1] << 0);
	return val;
}

void to_buffer32(uint32_t val, uint8_t * buf)
{
	buf[0] = (val >> 24) & 0xff;
	buf[1] = (val >> 16) & 0xff;
	buf[2] = (val >>  8) & 0xff;
	buf[3] = (val >>  0) & 0xff;
}

void to_buffer16(uint16_t val, uint8_t * buf)
{
	buf[0] = (val >> 8) & 0xff;
	buf[1] = (val >> 0) & 0xff;
}
