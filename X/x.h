#ifndef __X_H__  
#define __X_H__

#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "stm32f0xx.h"

#if !defined(NULL)
#define NULL	((void *)0)
#endif

enum {
	FALSE		= 0,
	TRUE		= 1,
};

#define ARRAY_SIZE(array)	( sizeof(array) / sizeof((array)[0]) )

#endif
