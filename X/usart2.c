#include "usart2.h"

int ct = 4, all = 0, clr[3], numct = 0;
uint8_t get_input;

extern int r[8],g[8], b[8],ledct, timeout;


void usart2_interrupt(void)
{
	USART_SendData(USART2, 'L');
	if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
	{
		timeout = 0;
		get_input = USART_ReceiveData(USART2);
		
		switch (ct)
		{
			case 4: //# of LED or all
				if (get_input == 0x61)//a
				{
					all = 1;
					ledct = 0;
					ct--;
					break;
				}
				else
				{
					all = 0;
					ledct = get_input - 0x30;
					ct--;
					break;	
				}
				
				
			case 3://space
				ct--;
				break;
			
			
			case 2://red value
				if (get_input == 0x20) //If space, get color and put in to RED array
				{
					if (all)
					{
						for (ledct = 0; ledct < 8; ledct++)
						{
							r[ledct] = get_color(clr, numct);
						}
					}
					else
					{
						r[ledct] = get_color(clr, numct);
					}
					ct--;
					numct = 0;
				}
				
				else //Store digits in to clr array
				{
					clr[numct] = get_input - 0x30;
					numct++;
				}
				break;
				//rrrrrrrrrrrrrrrrrrrrrrrr
				
				
				case 1://green value
				if (get_input == 0x20) //If space, get color and put in to GREEN array
				{
					if (all)
					{
						for (ledct = 0; ledct < 8; ledct++)
						{
							g[ledct] = get_color(clr, numct);
						}
					}
					else
					{
						g[ledct] = get_color(clr, numct);
					}
					numct = 0;
					ct--;
				}
				else //Store digits in to clr array
				{
					clr[numct] = get_input - 0x30;
					numct++;
				}
				break;
				//ggggggggggggggggggggggggg
				
				
				case 0: //blue value
			if (get_input == 0x20)
			{

				if (all)
				{
					for (ledct = 0; ledct < 8; ledct++)
					{
						b[ledct] = get_color(clr, numct);
					}
				}
				else
				{
					b[ledct] = get_color(clr, numct);
				}
				numct = 0;
				ct = 4;
				
			}
			else
			{
				clr[numct] = get_input-0x30;
				numct++;
			}
				break;
				//bbbbbbbbbbbbbbbbbbbbbbbbbb
		
		}
	}
}


int get_color(int clr[], int num_ct) //Calculate color from digits
{
	int color_temp;
	switch(num_ct)
	{
		case 1:
			color_temp = clr[0];
			break;
		case 2:
			color_temp = clr[0]*10 + clr[1];
			break;
		case 3:
			color_temp = clr[0]*100 + clr[1]*10 + clr[2];
			break;
		default:
			color_temp = 0;
			break;
	}
	
	return color_temp;
}



void usart2_initial(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* config USART2 clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	/* Connect pin to Periph */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_1);
  
	/* Configure pins as AF pushpull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_InitStructure); 

	/* USART2 mode config */
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART2, &USART_InitStructure);
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	USART_Cmd(USART2, ENABLE);

	/* Configure the NVIC Preemption Priority Bits */

	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0x01;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

uint32_t usart2_write(const uint8_t * buf, uint32_t count)
{
	uint32_t i;

	for(i = 0; i < count; i++)
	{
		USART_SendData(USART2, buf[i]);
		while(USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET);
	}

	return i;
}


