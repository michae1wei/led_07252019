#ifndef __X_USART2_H__  
#define __X_USART2_H__

#include "x.h"

void usart2_interrupt(void);
void usart2_initial(void);
int get_color(int clr[], int num_ct);
uint32_t usart2_write(const uint8_t * buf, uint32_t count);

#endif /* __X_USART2_H__ */
