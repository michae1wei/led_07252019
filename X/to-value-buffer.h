#ifndef __X_TO_VALUE_BUFFER_H__
#define __X_TO_VALUE_BUFFER_H__

#include "x.h"

uint32_t to_value32(uint8_t * buf);
uint16_t to_value16(uint8_t * buf);
void to_buffer32(uint32_t val, uint8_t * buf);
void to_buffer16(uint16_t val, uint8_t * buf);

#endif /* __X_TO_VALUE_BUFFER_H__ */
