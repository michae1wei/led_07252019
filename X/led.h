#ifndef __X_LED_H__  
#define __X_LED_H__

#include "x.h"

enum led_name_t {
	LED_NAME_A		= 1,
	LED_NAME_B		= 2,
};

enum led_status_t {
	LED_STATUS_OFF	= 0,
	LED_STATUS_ON	= 1,
};

void led_initial(void);
void led_set_status(enum led_name_t name, enum led_status_t status);

#endif /* __X_LED_H__ */
